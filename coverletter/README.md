# cover-letter
Plain-TeX Template for coverletters

The `coverletter-contents.tex` should be the main file edited, and then `coverletter.tex` you only need to modify when your address changes etc.
