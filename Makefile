# Compile the resume, cover-letter,
# and merge all them + transcripts.

NAME = blairdrummond

ANY_TRANSCRIPTS := $(shell ls -a | grep -i 'transcripts*.pdf')

RESUME = $(NAME)-resume.pdf
COVERLETTER = $(NAME)-coverletter-$(RECIPIENT).pdf
COMBINED = $(NAME)-$(RECIPIENT).pdf

RECIPIENTS-DIR = recipients
OUTPUT = output
BUILD-DIR = .build-dir

default: one-file
	@echo Default rule

$(OUTPUT):
	mkdir -p $(OUTPUT)

$(BUILD-DIR):
	mkdir -p $(BUILD-DIR)

build-resume: $(OUTPUT)
	@echo Building Resume
	cd resume && pdftex resume.tex
	@cp resume/resume.pdf $(OUTPUT)/$(RESUME)

build-coverletter: $(OUTPUT) $(BUILD-DIR)
	@echo
	$(eval RECIPIENT := $(shell ls $(RECIPIENTS-DIR) | sed 's/\.tex//' | slmenu))
	@echo Building Coverletter --- to: $(RECIPIENT)
	@cp -r coverletter/* $(BUILD-DIR)
	@cp $(RECIPIENTS-DIR)/$(RECIPIENT).tex $(BUILD-DIR)/coverletter-contents.tex
	cd $(BUILD-DIR) && pdftex coverletter.tex
	@cp $(BUILD-DIR)/coverletter.pdf $(OUTPUT)/$(COVERLETTER)
	@echo
	@echo Done. Wrote to $(OUTPUT)/$(COVERLETTER)
	@echo

.blank.pdf: .blank/blank.tex
	pdftex .blank/blank.tex && rm blank.log && mv blank.pdf .blank.pdf

one-file: $(OUTPUT) build-coverletter build-resume .blank.pdf
	@pdfunite $(OUTPUT)/$(COVERLETTER) .blank.pdf $(OUTPUT)/$(RESUME) $(ANY_TRANSCRIPTS) $(OUTPUT)/$(COMBINED)
	@echo 
	@echo Done. Wrote to $(OUTPUT)/$(COMBINED)
