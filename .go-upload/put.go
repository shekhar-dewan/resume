package main

import (
    "os"
    // Additional imports needed for examples below
    "fmt"
	"bytes"
	"net/http"
	"flag"

    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/credentials"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/service/s3"
)

func myUsage() {
     fmt.Printf("Usage: %s [OPTIONS] infile s3_outpath\n\nOptions:\n", os.Args[0])
     flag.PrintDefaults()
}

func main() {

	// env variables
    key := os.Getenv("SPACES_KEY")
    secret := os.Getenv("SPACES_SECRET")
	if key == "" || secret == "" {
		fmt.Println("SPACES_KEY and SPACES_SECRET must be set.")
		os.Exit(1)
	}

	// defaults
	flag.Usage = myUsage
	bucketPtr := flag.String("bucket", "happylittlecloud", "Bucket")
	endpointPtr := flag.String(
		"endpoint",
		"https://nyc3.digitaloceanspaces.com",
		"Endpoint (default: https://nyc3.digitaloceanspaces.com",
	)
    accessPtr := flag.String("access", "private", "private or public-read  (default: private)")
    flag.Parse()

	// I will make this mistake 1000 times
	if *accessPtr == "public" {
		*accessPtr = "public-read"
	}

	if *accessPtr != "public-read" && *accessPtr != "private" {
		fmt.Println("Error: access may only be public-read or private")
		os.Exit(1)
	}

	// infile and outdest must remain
	if flag.NArg() != 2 {
		flag.Usage()
		os.Exit(1)
	}
	infile := flag.Arg(0)
	outdest := flag.Arg(1)


    s3Config := &aws.Config{
        Credentials: credentials.NewStaticCredentials(key, secret, ""),
        Endpoint:    aws.String(*endpointPtr),
        Region:      aws.String("us-east-1"),
    }

    s3Client, err := session.NewSession(s3Config)
    if err != nil {
		fmt.Println(err.Error())
    }

    // Open the file for use
    file, err := os.Open(infile)
    if err != nil {
		fmt.Println(err.Error())
    }
    defer file.Close()

    // Get file size and read the file content into a buffer
    fileInfo, _ := file.Stat()
    var size int64 = fileInfo.Size()
    buffer := make([]byte, size)
    file.Read(buffer)

    // Config settings: this is where you choose the bucket, filename, content-type etc.
    // of the file you're uploading.
    _, err = s3.New(s3Client).PutObject(&s3.PutObjectInput{
        Bucket:               aws.String(*bucketPtr),
        Key:                  aws.String(outdest),
        ACL:                  aws.String(*accessPtr),
        Body:                 bytes.NewReader(buffer),
        ContentLength:        aws.Int64(size),
        ContentType:          aws.String(http.DetectContentType(buffer)),
        ContentDisposition:   aws.String("attachment"),
    })

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
